import { defineStore, acceptHMRUpdate } from "pinia";

export const useLayoutStore = defineStore({
  id: "layout",
  state: () => ({
    rightDrawerStatus: false,
    rightDrawerComponent: null,
    newTaskModalStatus: false,
  }),

  actions: {
    openRightDrawer(componentName) {
      this.rightDrawerComponent = componentName;
      this.rightDrawerStatus = true;
    },
    closeRightDrawer() {
      this.rightDrawerStatus = false;
      this.rightDrawerComponent = null;
    },
    openNewTaskModal() {
      this.newTaskModalStatus = true;
    },
    closeNewTaskModal() {
      this.newTaskModalStatus = false;
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useLayoutStore, import.meta.hot));
}
