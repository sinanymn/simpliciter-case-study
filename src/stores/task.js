import { defineStore, acceptHMRUpdate } from "pinia";
import axios from "axios";

export const useTaskStore = defineStore({
  id: "task",
  state: () => ({
    task: null,
    tasks: [],
    filterTerm: "",
    filterTitle: "",
    filterTags: [],
    filterState: null,
  }),

  getters: {
    filtetedList: (state) =>
      state.tasks.filter(
        (_t) =>
          _t.title
            .toLowerCase()
            .search(state.filterTitle.toLowerCase().trim()) !== -1 &&
          (state.filterTags.length > 0
            ? _t.tags.some((_tag) => state.filterTags.includes(_tag))
            : true) &&
          (state.filterState ? _t.state === state.filterState : true)
      ),
    allTags: (state) => [...new Set(state.tasks.map((_t) => _t.tags).flat())],
    completedTasksList: (state) =>
      state.tasks.filter((_t) => _t.state === "completed"),
  },

  actions: {
    async fetchTasks() {
      try {
        const { data } = await axios.get(`/data.json`);
        this.tasks = data;
      } catch (e) {
        throw new Error(e.toString());
      }
    },
    async fetchTask(id) {
      try {
        //for dummy data
        let task;
        if (this.tasks.length > 0) {
          task = this.tasks.find((_t) => _t.id === id);
        } else {
          const { data } = await axios.get(`/data.json`);
          task = data.find((_t) => _t.id === id);
        }
        if (!task) {
          throw new Error("Task not found!");
        } else {
          this.task = task;
        }
      } catch (e) {
        throw new Error(e.toString());
      }
    },
    setTask(task) {
      this.task = task;
    },
    updateTask(task) {
      const taskIndex = this.tasks.findIndex((_t) => _t.id === task.id);
      if (taskIndex !== -1) {
        this.tasks[taskIndex] = task;
      }
    },
    addTask(task) {
      task.id = Date.now().toString();
      this.tasks.unshift(task);
    },
    updateTaskStatus(id, status) {
      const taskIndex = this.tasks.findIndex((_t) => _t.id === id);
      if (taskIndex !== -1) {
        this.tasks[taskIndex].state = status;
      }
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useTaskStore, import.meta.hot));
}
