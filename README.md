# Simpliciter Case Study

This is a case study for Simpliciter.

## Online Demo

[https://simpliciter-case-study.netlify.app/](https://simpliciter-case-study.netlify.app/)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with ESLint

```sh
npm run lint
```
