import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "TaskList",
      component: () => import("../views/TaskList.vue"),
    },
    {
      path: "/task/:id",
      name: "TaskDetail",
      component: () => import("../views/TaskDetail.vue"),
    },
  ],
});

export default router;
